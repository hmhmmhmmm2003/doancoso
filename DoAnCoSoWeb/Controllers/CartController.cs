﻿using DoAnCoSoWeb.Models;
using DoAnCoSoWeb.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DoAnCoSoWeb.Controllers
{
	public class CartController : Controller
	{
		private readonly ApplicationDbContext _context;
		private const string CartSession = "CartSession";
		public CartController(ApplicationDbContext context)
		{
			_context = context;
		}

		public async Task<IActionResult> Index()
		{			
			var cart = HttpContext.Session.GetString(CartSession);
			var list = new List<ChiTietGioHang>();
			if (!string.IsNullOrEmpty(cart))
			{
				list = JsonConvert.DeserializeObject<List<ChiTietGioHang>>(cart);
			}
			var cartViewModel = new CartViewModels
			{
				chiTietGioHangs = list
			};
			return View(cartViewModel);
		}

		public IActionResult AddItem(int ProductId, int Quantity)
		{
			var product = _context.sanphams.Find(ProductId);
			var cart = HttpContext.Session.GetString(CartSession);

			if (!string.IsNullOrEmpty(cart))
			{
				var list = JsonConvert.DeserializeObject<List<ChiTietGioHang>>(cart);

				if (list.Exists(x => x.SanPhams.Id == ProductId))
				{
					foreach (var item in list)
					{
						if (item.SanPhams.Id == ProductId)
						{
							item.Quantity += Quantity;
						}
					}
				}
				else
				{
					var item = new ChiTietGioHang
					{
						SanPhams = product,
						Quantity = Quantity
					};

					list.Add(item);
				}

				HttpContext.Session.SetString(CartSession, JsonConvert.SerializeObject(list));
			}
			else
			{
				var item = new ChiTietGioHang
				{
					SanPhams = product,
					Quantity = Quantity
				};

				var list = new List<ChiTietGioHang> { item };

				HttpContext.Session.SetString(CartSession, JsonConvert.SerializeObject(list));
			}

			return RedirectToAction("Index");
		}


		public IActionResult DeleteAll()
		{
			HttpContext.Session.Remove(CartSession);
			return Json(new
			{
				status = true
			});
		}

		public IActionResult Delete(int id)
		{
			var sessionCart = JsonConvert.DeserializeObject<List<ChiTietGioHang>>(HttpContext.Session.GetString(CartSession));
			sessionCart.RemoveAll(x => x.SanPhams.Id == id);
			HttpContext.Session.SetString(CartSession, JsonConvert.SerializeObject(sessionCart));
			return Json(new
			{
				status = true
			});
		}

		public IActionResult Update(string cartModel)
		{
			var jsonCart = JsonConvert.DeserializeObject<List<ChiTietGioHang>>(cartModel);
			var sessionCart = JsonConvert.DeserializeObject<List<ChiTietGioHang>>(HttpContext.Session.GetString(CartSession));

			foreach (var item in sessionCart)
			{
				var jsonItem = jsonCart.SingleOrDefault(x => x.SanPhams.Id == item.SanPhams.Id);
				if (jsonItem != null)
				{
					item.Quantity = jsonItem.Quantity;
				}
			}
			HttpContext.Session.SetString(CartSession, JsonConvert.SerializeObject(sessionCart));
			return Json(new
			{
				status = true
			});
		}

		
	}
}
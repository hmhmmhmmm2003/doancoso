﻿using DoAnCoSoWeb.Models;
using DoAnCoSoWeb.Repository;
using DoAnCoSoWeb.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Drawing.Printing;

namespace DoAnCoSoWeb.Controllers
{
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly IProductRepository _productRepository;

        public HomeController (ApplicationDbContext context)
        { 
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            var sanpham = await _context.sanphams.ToListAsync();

            var viewModel = new HomeViewModels
            {
                sanphams = sanpham,
            };

            return View(viewModel);
            /*return View();*/
        }

        [HttpGet]
        public IActionResult IsLoggedIn()
        {
            var status = User.Identity.IsAuthenticated;
            return Json(new { status = status });
        }

        /*public async Task<IActionResult> ProdDetail(string slug, long id)
        {
            var prods = await _context.sanphams.Where(m => m.Link == slug && m.Id == id).ToListAsync();
            if (prods == null)
            {
                var errorViewModel = new ErrorViewModel
                {
                    RequestId = "Product Error",
                };
                return View("Error", errorViewModel);
            }

            var viewModel = new HomeViewModels
            {
                sanphams = prods,
            };

            return View(viewModel);
        }*/
        public async Task<IActionResult> ProdDetail(long id)
        {
            var product = await _context.sanphams.FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                var errorViewModel = new ErrorViewModel
                {
                    RequestId = "Product Error",
                };
                return View("Error", errorViewModel);
            }

            var viewModel = new HomeViewModels
            {
                sanphams = new List<Sanpham> { product },
            };

            return View(viewModel);
        }


    }
}

﻿using DoAnCoSoWeb.Models;

namespace DoAnCoSoWeb.ViewModels
{
    public class HomeViewModels
    {
        public List<Sanpham> sanphams { get; set; }
        public List<Sale> sale { get; set; }
    }
}

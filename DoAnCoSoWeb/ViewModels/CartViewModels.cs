﻿using DoAnCoSoWeb.Models;

namespace DoAnCoSoWeb.ViewModels
{
    public class CartViewModels
    {
        public List<Giohang> giohangs {  get; set; }
        public List<ChiTietGioHang> chiTietGioHangs { get; set; }
    }
}

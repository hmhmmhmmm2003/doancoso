﻿namespace DoAnCoSoWeb.Models
{
	public class Comment
	{
		public int Id { get; set; }

		public int AccountId { get; set; }
		public Account? Account { get; set; }

		public List<ChiTietGioHang> chiTietGioHangs { get; set; }
	}
}

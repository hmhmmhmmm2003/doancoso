﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoWeb.Models
{
    public class ChiTietHoaDon
    {
        [Key]
        public int Id { get; set; }
        public List<Sanpham>? SanPham { get; set; }

        public int HoaDonId { get; set; }
        public Hoadon? Hoadon { get; set; }

        [Range(1, int.MaxValue)]
        public int SoLuong { get; set; }

        [Range(0, 100000000.00)]
        public decimal ThanhTien { get; set; }
    }


}
